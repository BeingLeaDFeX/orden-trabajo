import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SheetOTComponent } from './sheet-ot.component';

describe('SheetOTComponent', () => {
  let component: SheetOTComponent;
  let fixture: ComponentFixture<SheetOTComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SheetOTComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SheetOTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
