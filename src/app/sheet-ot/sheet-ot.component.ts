import { Component, OnInit } from '@angular/core';

import { AsistenciaService } from '../services/asistencia/asistencia.service';
import { Asistencia } from '../services/asistencia/asistencia';

@Component({
  selector: 'sheet-ot',
  templateUrl: './sheet-ot.component.html',
  styleUrls: ['./sheet-ot.component.scss']
})
export class SheetOTComponent implements OnInit {
  asistencias: Asistencia[];

  constructor(private asistenciaService: AsistenciaService) {}

  ngOnInit() {
    this.getAsistencias();
  }

  getAsistencias():void {
    this.asistenciaService.getAsistencias()
      .subscribe(asistencias => this.asistencias = asistencias);
    //this.asistencias = this.asistenciaService.getAsistencias();
  }

}
