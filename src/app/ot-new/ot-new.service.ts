import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Cliente } from './ot-new';
import { HttpErrorHandler, HandleError } from '../http/http-error-handler.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class OTNewService {
  Url = 'api/neworder';  // URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler
  ) {
    this.handleError = httpErrorHandler.createHandleError('OTNewService');
  }

  /** POST: add a new hero to the database */
  addCliente (cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.Url, cliente, httpOptions)
      .pipe(
        catchError(this.handleError('addCliente', cliente))
      );
  }


}
