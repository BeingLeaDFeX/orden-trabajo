export interface Cliente {
  nombre: string;
  rut: string;
  direccion: string;
  comuna: string;
  tel1: string;
  tel2: string;
  tel3: string;
  web1: string;
  web2: string;
  web3: string;
  sitio_id: string;
  sitio_dir: string;
  sitio_cmna: string;
  sitio_tel1: string;
  sitio_tel2: string;
  sitio_tel3: string;
}
