import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtNewComponent } from './ot-new.component';

describe('OtNewComponent', () => {
  let component: OtNewComponent;
  let fixture: ComponentFixture<OtNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
