import { Component, OnInit, Inject } from '@angular/core';
//validacion de formulario
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
//ventana emergente
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
//http
import { Config, ConfigService } from './config.service';
import { HttpMessageService } from '../http/http-message.service';

import { User } from './user';
import { UsersService } from './users.service';

import { Cliente } from './ot-new';
import { OTNewService } from './ot-new.service';
//import { MessagesComponent } from '../messages/messages.component';

/* INTERFAZ DE INPUTS SELECT (boton de seleccion) */
export interface Respuesta {
  value: string;
  viewValue: string;
}

/* INTERFAZ DE DIALOGO (ventana emergente) */
export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'ot-new',
  templateUrl: './ot-new.component.html',
  providers: [
    ConfigService,
    HttpMessageService,
    UsersService,
    OTNewService,
  ],
  styleUrls: ['./ot-new.component.scss']
})
export class OtNewComponent implements OnInit {
  // Variables http
  error: any;
  headers: string[];
  config: Config;

  users: User[];
  clientes: Cliente[];
  newCliente: Cliente;

  /* VARIABLES DE DIALOGO (ventana emergente)*/
  animal: string;
  name: string;

  /* FUNCIONES FORM BULDER */
  OTForm = this.fb.group(
    {
    Cliente: this.fb.group(
      {
      nombre: ['', Validators.required],
      direccion: [''],
      comuna: [''],
      web: ['']
      }
    ),
    Mandante: this.fb.group({
      nombre: ['', Validators.required],
      telefono: ['', Validators.required],
      correo: ['', Validators.required, Validators.email],
      direccion: ['']
    }),
    Solicitud: this.fb.group({
      contrato: [''],
      sitio: ['', Validators.required],
      caso: ['', Validators.required]
    }),
    Respuesta: this.fb.group({
      tipo: ['', Validators.required],
      agenda: ['', Validators.required],
      personal: [''],
      herramientas: ['']
    })
  }
  );

  Controls = {
    Cliente: {
      nombre: (this.OTForm.controls.Cliente as FormGroup).controls.nombre,
      direccion: (this.OTForm.controls.Cliente as FormGroup).controls.direccion,
      comuna: (this.OTForm.controls.Cliente as FormGroup).controls.comuna,
      web: (this.OTForm.controls.Cliente as FormGroup).controls.web,
    },
    Mandante: {
      nombre: (this.OTForm.controls.Mandante as FormGroup).controls.nombre,
      telefono: (this.OTForm.controls.Mandante as FormGroup).controls.telefono,
      correo: (this.OTForm.controls.Mandante as FormGroup).controls.correo,
      direccion: (this.OTForm.controls.Mandante as FormGroup).controls.direccion,
    },
    Solicitud: {
      contrato: (this.OTForm.controls.Solicitud as FormGroup).controls.contrato,
      sitio: (this.OTForm.controls.Solicitud as FormGroup).controls.sitio,
      caso: (this.OTForm.controls.Solicitud as FormGroup).controls.caso,
    },
    Respuesta: {
      tipo: (this.OTForm.controls.Respuesta as FormGroup).controls.tipo,
      agenda: (this.OTForm.controls.Respuesta as FormGroup).controls.agenda,
      personal: (this.OTForm.controls.Respuesta as FormGroup).controls.personal,
      herramientas: (this.OTForm.controls.Respuesta as FormGroup).controls.herramientas,
    },
  };

  Errors = {
    Cliente: {
      nombre: this.Controls.Cliente.nombre
                .hasError('required') ? 'Por favor ingresa el nombre' :
                '',
      direccion: '',
      comuna: '',
      web: '',
      },
      Mandante: {
        nombre: this.Controls.Mandante.nombre.hasError('required') ? 'Por favor ingresa el nombre' :
                '',
        telefono: this.Controls.Mandante.telefono.hasError('required') ? 'Por favor ingresa el telefono' :
                '',
        correo: this.Controls.Mandante.nombre.hasError('required') ? 'Por favor ingresa el correo' :
                this.Controls.Mandante.nombre.hasError('email') ? 'No es un email válido' :
                '',
        direccion: '',

      },
      Solicitud: {
        contrato: '',
        sitio: this.Controls.Solicitud.sitio
                .hasError('required') ? 'Debes ingresar el lugar donde concurriremos' :
                '',
        caso: this.Controls.Solicitud.caso
                .hasError('required') ? 'Describe la solicitud por favor' :
                '',
      },
      Respuesta: {
        tipo: this.Controls.Respuesta.tipo
                .hasError('required') ? 'Indica si iremos a terreno o no' :
                '',
        agenda: this.Controls.Respuesta.agenda
                .hasError('required') ? 'Indica una fecha, aunque sea tentativa' :
                '',
        personal: '',
        herramientas: '',
      },
  };

  get aliases() {
    return this.OTForm.get('aliases') as FormArray;
  }

/* CONTROL DE APERTURA DE ACORDIOANES */
  step = 0;
  todos = 0;
  revision: boolean = true;
  cancelarLabel: string = "Editar";
  leerSolo: boolean = false;

  setOpenAll (index: number) {
    //if(this.houston_estamos_listos()){
      this.step = index;
      this.todos = 1;
      this.revision = false;
      this.leerSolo = true;
    //  this.solCaso.disable();
    //  }
  }

  setStep(index: number) {
   this.step = index;
  }

  nextStep() {
   this.step++;
   this.todos = 0;
  }

  prevStep() {
   this.step--;
   this.todos = 0;
  }

  editar() {
    this.revision = true;
    this.todos = 0;
    this.step = 0;
    this.leerSolo = false;
  }

/* VALORES DE INPUTS OPCIONES */
  selectedValue: string;
  respuestas: Respuesta[] = [
    {value: '1', viewValue: 'Sin definir'},
    {value: '2', viewValue: 'Remoto'},
    {value: '3', viewValue: 'Terreno'}
  ];



  constructor(
    private fb: FormBuilder, //formulario
    public dialog: MatDialog, //ventana emergente
    private configService: ConfigService, //http
    private usersService: UsersService,
    private otnewService: OTNewService,
  ) { }

  //Funciones https
  clear() {
    this.config = undefined;
    this.error = undefined;
    this.headers = undefined;
  }

  showConfig() {
    this.configService.getConfig()
      .subscribe(
        (data: Config) => this.config = { ...data }, // success path
        error => this.error = error // error path
      );
  }


  /* FUNCIONES DEL DIALOGO (ventana emergente) */
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewOTCreated, {
      width: '250px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  /* FUNCIONES DE CONTROL DE FORM */
  updateProfile() {
    this.OTForm.patchValue({

    });
  }

  addAlias() {
    this.aliases.push(this.fb.control(''));
  }

  onSubmit() {
    console.log(this.OTForm.value);

    var nombre = this.OTForm.value.Cliente.nombre;
    var direccion = this.OTForm.value.Cliente.direccion;
    var comuna = this.OTForm.value.Cliente.comuna;
    var web1 = this.OTForm.value.Cliente.web;

    this.newCliente = {
      nombre, direccion, comuna, web1
    } as Cliente;

    console.log(this.newCliente);


    this.addCliente10();
  }

  consoleLog() {
      console.log("Oli!!");
  }

  ngOnInit() {
    //this.getUsers();
  }

  getUsers(): void {
    this.usersService.getUsers()
      .subscribe(users => this.users = users);
  }

  addUser(name: string, email: string): void {
    const newUser: User = { name, email } as User;
    this.usersService.addUser(newUser)
      .subscribe(user => this.users.push(user));
  }

  addCliente10(): void {
    this.otnewService.addCliente(this.newCliente)
      .subscribe(cliente => this.clientes.push(cliente));
  }

  addCliente20(
    nombre: string,
    rut: string,
    direccion: string,
    comuna: string,
    tel1: string,
    tel2: string,
    tel3: string,
    web1: string,
    web2: string,
    web3: string,
    sitio_id: string,
    sitio_dir: string,
    sitio_cmna: string,
    sitio_tel1: string,
    sitio_tel2: string,
    sitio_tel3: string,
  ): void {
    const newCliente: Cliente = {
      nombre,
      rut,
      direccion,
      comuna,
      tel1,
      tel2,
      tel3,
      web1,
      web2,
      web3,
      sitio_id,
      sitio_dir,
      sitio_cmna,
      sitio_tel1,
      sitio_tel2,
      sitio_tel3,
    } as Cliente;
    this.otnewService.addCliente(newCliente)
      .subscribe(cliente => this.clientes.push(cliente));
  }

}

/* COMPONENTE INYECTABLE DE DIALOGO */

@Component({
  selector: 'dialog-overview-OTCreated',
  templateUrl: './dialog-overview-OTCreated.html',
})
export class DialogOverviewOTCreated {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewOTCreated>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
