import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Router } from '@angular/router';
import { User, UserService,
    //AuthenticationService,
    AutenticacionService
  } from '../login';

@Component({
  selector: 'main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent {
  auth: any;

  isHandset$: Observable<boolean> =
    this.breakpointObserver.observe(Breakpoints.Handset)
      .pipe(
        map(result => result.matches)
      );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private _router: Router,
    private _auth: AutenticacionService
  ) { }

  ngOnInit() {

  }

  puedoVerMenu(): boolean{
    if(this._router.url != '/login') return true
    return false
  }

  getUrl(): string {
    return this._router.url;
  }

  logout() {
      this._auth.logout();
      this._router.navigate(['/login']);
  }

}
