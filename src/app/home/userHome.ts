export class UserHome {
    user_id: number;
    email: string;
    last_login: any;
    nombres: string;
    apellidos: string;
    tel1: string;
}
/*
[{"user_id":6,
  "email":"pablo@example.com",
  "last_login":"2019-07-05T05:29:34.515Z",
  "nombres":"pablo",
  "apellidos":"pablo",
  "tel1":null},

  {"user_id":1,"email":"test@example.com","last_login":"2019-07-03T04:21:35.453Z","nombres":"test","apellidos":"test","tel1":null}]
*/
