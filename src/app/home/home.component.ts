import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import {
         Usuario,
         UsuarioService,
  } from '../login';

import { HttpMessageService } from '../http/http-message.service';
import { UserHome } from './userHome';
import { UserHomeService } from './userHome.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  providers: [
    UserHomeService,
    HttpMessageService
  ],
  styleUrls: ['./home.component.scss']
})

export class HomeComponent {
  loading = false;
  users: UserHome[];
  //usuarios: Usuario[];

  constructor( //private userService: UserService,
               //private usuarioService: UsuarioService,
               private userHomeService: UserHomeService
  ) { }

  ngOnInit() {
      this.loading = true;
      this.getUsersHome();
      console.log("HomeComponent: "+this.users);
/*
      this.usuarioService.getUsuarios().pipe(first())
        .subscribe(usuarios => {
          this.loading = false;
          this.usuarios = usuarios;
        });
        console.log("home component (Aqui 2)");
        console.log(JSON.stringify(this.usuarios));*/
  }

  getUsersHome() {
      this.userHomeService.getUsers()
        .subscribe ( users => {
          this.loading = false;
          this.users = users;
        });
  }

  actua() {
    this.userHomeService.getUsers();
  }
}
