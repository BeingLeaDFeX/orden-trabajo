import { Injectable , OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { AutenticacionService } from '../login/_services';

import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { UserHome } from './userHome';
import { HttpErrorHandler, HandleError } from '../http/http-error-handler.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({ providedIn: 'root' })
export class UserHomeService {
  Url = 'api/home';  // URL to web api
  private handleError: HandleError;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private autenticacionService: AutenticacionService,
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler
  ) {
    this.handleError = httpErrorHandler.createHandleError('userHomeService');
  }

  getUsers(): Observable<UserHome[]> {
    if (isPlatformBrowser(this.platformId)) {
      let currentUser = this.autenticacionService.currentUsuarioValue;
      let url = this.Url+"/:"+currentUser.token;
      console.log("userHomeService(): "+ url);
      return this.http.get<UserHome[]>(url)
        .pipe(
          catchError(this.handleError('HomeComponent getUsers()', []))
        );
    }
  }

}
