import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from "@angular/common";

import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

//import { environment } from '../../../environments/environment';
import { Usuario } from './usuario';

@Injectable({ providedIn: 'root' })

export class AutenticacionService {

  private currentUsuarioSubject: BehaviorSubject<Usuario>;
  public currentUsuario: Observable<Usuario>;

  loginUrl = 'api/login';

  constructor(
              private http: HttpClient,
              @Inject(PLATFORM_ID) private platformId: Object
  ) {
    if (isPlatformBrowser(this.platformId)) {
      this.currentUsuarioSubject = new BehaviorSubject<Usuario>(
        JSON.parse(localStorage.getItem('currentUser')));
      this.currentUsuario = this.currentUsuarioSubject.asObservable();

      console.log("AutenticacionService.constructor()"
        +JSON.parse(localStorage.getItem('currentUser')));
    }
  }

  public get currentUsuarioValue(): Usuario {
    if (isPlatformBrowser(this.platformId)) {
      return this.currentUsuarioSubject.value;
    }
  }

  login(_username: string, _password: string) {

    if (isPlatformBrowser(this.platformId)) {
      //console.log("autenticacionService login()");

      return this.http.post<any>(`${this.loginUrl}`, { _username, _password })
        .pipe(map(user => {
          // store Usuario details and jwt token in local storage to keep
          // Usuario logged in between page refreshes
          localStorage.setItem('currentUsuario', JSON.stringify(user));
          this.currentUsuarioSubject.next(user);

          console.log("autenticacionService login() user: "+ JSON.stringify(user));

          return user;
      }));
    }
  }

  logout() {
    if (isPlatformBrowser(this.platformId)) {
      // remove Usuario from local storage to log Usuario out
      localStorage.removeItem('currentUsuario');
      this.currentUsuarioSubject.next(null);
    }
  }
}
