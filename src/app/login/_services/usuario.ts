export class Usuario {
    user_id: number;
    email: string;
    last_login: any;
    nombres:string;
    apellidos: string;
    tel1: string;
    token?: string;
}
