import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import {isPlatformBrowser} from "@angular/common";
import { Router,
         CanActivate,
         ActivatedRouteSnapshot,
         RouterStateSnapshot
} from '@angular/router';

import { AutenticacionService } from './autenticacion.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private router: Router,
    private autenticacionService: AutenticacionService
    ) { }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot )
    {
    if (isPlatformBrowser(this.platformId)) {
      const currentUser = this.autenticacionService.currentUsuarioValue;
      console.log("auth.guard.canActivate(): "+JSON.stringify(currentUser));
      if (currentUser) {
        // logged in so return true
        return true;
      }
      // not logged in so redirect to login page with the return url
      console.log("auth guard canActivate(): false");
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }
}
