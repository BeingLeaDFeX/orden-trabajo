import { Injectable, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Usuario } from './usuario';
import { HttpErrorHandler, HandleError
  } from '../../http/http-error-handler.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({ providedIn: 'root' })
export class UsuarioService {
  loginUrl = 'api/login';  // URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler
  ) {
    this.handleError = httpErrorHandler.createHandleError('getUsuarios');
  }

  getUsuarios (): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.loginUrl)
      .pipe(
        catchError(this.handleError('getUsuarios', []))
      );
  }

  /** POST: add a new hero to the database */
/*  addCliente (usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.loginUrl, usuario, httpOptions)
      .pipe(
        catchError(this.handleError('postUsuarios', usuario))
      );
  }

*/
}
