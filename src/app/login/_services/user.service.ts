import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

//import { environment } from '../../../environments/environment';
import { User } from './user';

@Injectable({ providedIn: 'root' })
export class UserService {
  environment = {
    production: false,
    apiUrl: 'http://localhost:4200'
  };

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<User[]>(`${this.environment.apiUrl}/users`);
  }
}
