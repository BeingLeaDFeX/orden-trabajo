import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import {
//         AuthenticationService,
         AutenticacionService
  } from '../login/_services';

@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
//  private authenticationService: AuthenticationService,
    private autenticacionService: AutenticacionService
    ) {
    // redirect to home if already logged in
    if (this.autenticacionService.currentUsuarioValue) {
      console.log("login component constructor");
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    console.log('login componente returnUrl: '+this.returnUrl);
  }

  // convenience getter for easy access to form fields
  get f() {
    console.log("login componente get f");
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    // new auth
    this.loading = true;
    this.autenticacionService.login(this.f.username.value, this.f.password.value)
      .pipe(first()).subscribe(
        data => {
          console.log("login componente onSubmit(): "+this.returnUrl);
          this.router.navigate([this.returnUrl]);
        },
        error => {
          console.log("login componente onSubmit(): error");
          this.error = error;
          this.loading = false;
        });
        // old auth
  /*      this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });*/
    }
}
