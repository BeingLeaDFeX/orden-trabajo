import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import {isPlatformBrowser} from "@angular/common";
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AutenticacionService } from '../_services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
              private autenticacionService: AutenticacionService,
              @Inject(PLATFORM_ID) private platformId: Object
            ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    if (isPlatformBrowser(this.platformId)) {

      console.log("JwtInterceptor");

      let currentUser = this.autenticacionService.currentUsuarioValue;

      if (currentUser && currentUser.token) {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${currentUser.token}`
          }
        });
      }
      try {
        console.log("JwtInterceptor(): "+JSON.stringify(currentUser.token));
      } catch {
        console.log("JwtInterceptor(): currentUser don't exist");
      }
      return next.handle(request);
    }
  }
}
