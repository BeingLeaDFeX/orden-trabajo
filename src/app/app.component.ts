import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Router } from '@angular/router';
import {
  AutenticacionService,
  Usuario
  } from './login/_services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ordenTrabajo';
  currentUsuario: Usuario;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private router: Router,
    private autenticacionService: AutenticacionService,
  ) {
    if (isPlatformBrowser(this.platformId)) {
    this.autenticacionService.currentUsuario
      .subscribe(x => this.currentUsuario = x);

    }}

  ngOnInit() {
    // Client only code.
/*    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('currentUsuario',
        JSON.stringify(this.currentUsuario));
    }*/
  }

    logout() {
          this.autenticacionService.logout();
          this.router.navigate(['/login']);
      }
}
