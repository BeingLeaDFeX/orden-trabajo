import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SheetOTComponent } from './sheet-ot/sheet-ot.component';
import { OtNewComponent } from './ot-new/ot-new.component';
import { OtMisComponent } from  './ot-mis/ot-mis.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent, AuthGuard } from './login';
import { TestComponent } from './test/test.component';
//import { MainNavComponent } from './main-nav/main-nav.component';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },

  //{ path: '', redirectTo: '/otNew', pathMatch: 'full' },
  { path: 'sheetOT', component: SheetOTComponent, canActivate: [AuthGuard] },
  { path: 'otNew', component: OtNewComponent, canActivate: [AuthGuard] },
  { path: 'otMis', component: OtMisComponent, canActivate: [AuthGuard] },
  { path: 'test', component: TestComponent, canActivate: [AuthGuard] },
  //{ path: 'menu', component: MainNavComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
