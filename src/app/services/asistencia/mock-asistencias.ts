import { Asistencia } from './asistencia';

export const ASISTENCIAS: Asistencia[] = [
  { id:1,
    nombre:'G. Cabrera',
    fecha:'Lunes, 14/05/19 - 9:36:25 AM',
    detalle:'In AngularJS, the ng-class directive includes/excludes CSS classes based on an expression. That expression is often a key-value control object with each key of the object defined as a CSS class name, and each value defined as a template expression that evaluates to a Boolean value.'
  },
  { id:2,
    nombre:'A. Zuñiga',
    fecha:'Lunes, 14/05/19 - 9:36:25 AM',
    detalle:'In AngularJS, the ng-class directive includes/excludes CSS classes based on an expression. That expression is often a key-value control object with each key of the object defined as a CSS class name, and each value defined as a template expression that evaluates to a Boolean value.'
  },
  { id:3,
    nombre:'S. Troncoso',
    fecha:'Lunes, 14/05/19 - 9:36:25 AM',
    detalle:'In AngularJS, the ng-class directive includes/excludes CSS classes based on an expression. That expression is often a key-value control object with each key of the object defined as a CSS class name, and each value defined as a template expression that evaluates to a Boolean value.'
  },  
  { id:4,
    nombre:'G. Cabrera',
    fecha:'Lunes, 14/05/19 - 9:36:25 AM',
    detalle:'In AngularJS, the ng-class directive includes/excludes CSS classes based on an expression. That expression is often a key-value control object with each key of the object defined as a CSS class name, and each value defined as a template expression that evaluates to a Boolean value.'
  },
  {
    id:5,
    nombre:'J. Acosta',
    fecha:'Lunes, 14/05/19 - 9:36:25 AM',
    detalle:'In AngularJS, the ng-class directive includes/excludes CSS classes based on an expression. That expression is often a key-value control object with each key of the object defined as a CSS class name, and each value defined as a template expression that evaluates to a Boolean value.'
  }


];
