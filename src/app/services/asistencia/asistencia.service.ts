import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Asistencia } from './asistencia';
import { ASISTENCIAS } from './mock-asistencias';

@Injectable({
  providedIn: 'root'
})
export class AsistenciaService {

  constructor() { }

  getAsistencias(): Observable<Asistencia[]> {
    return of(ASISTENCIAS);
  }

}
