import { TestBed } from '@angular/core/testing';

import { MisOtsService } from './mis-ots.service';

describe('MisOtsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MisOtsService = TestBed.get(MisOtsService);
    expect(service).toBeTruthy();
  });
});
