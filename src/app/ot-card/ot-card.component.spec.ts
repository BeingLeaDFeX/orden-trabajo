import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtCardComponent } from './ot-card.component';

describe('OtCardComponent', () => {
  let component: OtCardComponent;
  let fixture: ComponentFixture<OtCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
