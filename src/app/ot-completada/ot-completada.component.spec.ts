import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtCompletadaComponent } from './ot-completada.component';

describe('OtCompletadaComponent', () => {
  let component: OtCompletadaComponent;
  let fixture: ComponentFixture<OtCompletadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtCompletadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtCompletadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
