import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';

export interface Respuesta {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'ot-new2',
  templateUrl: './ot-new2.component.html',
  styleUrls: ['./ot-new2.component.scss']
})
export class OtNew2Component implements OnInit {

  /* FUNCIONES FORM BULDER */
  OTForm = this.fb.group(
    {
    Cliente: this.fb.group(
      {
      nombre: ['', Validators.required],
      Nombre: ['', Validators.required],
      Direccion: [''],
      Comuna: ['', Validators.required],
      Web: ['']
      }
    ),
    Mandante: this.fb.group({
      Nombre: ['', Validators.required],
      Telefono: [''],
      Correo: ['', Validators.email],
      Direccion: ['']
    }),
    Solicitud: this.fb.group({
      Contrato: [''],
      Sitio: [''],
      Caso: ['']
    }),
    Respuesta: this.fb.group({
      Tipo: [''],
      Agenda: [''],
      Personal: [''],
      Herramientas: ['']
    }),
    aliases: this.fb.array([
      this.fb.control('')
    ])
  }
  );

getError(){
  return this.OTForm.getError('Cliente');
}



    get aliases() {
      return this.OTForm.get('aliases') as FormArray;
    }

/* FORM CONTROL */
  cliNombre = new FormControl('', Validators.required);
  cliNombreError() {
    return this.manNombre.hasError('required') ? 'You must enter a value' :
      '';
  }
//  cliDireccion;
//  cliWeb;
  solSitio = new FormControl('', Validators.required);
  solSitioError() {
    return this.solSitio.hasError('required') ? 'Debes ingresar el lugar' :
      '';
  }
  solCaso = new FormControl('', Validators.required);

  solCasoError() {
    return this.solCaso.hasError('required') ? 'Debes especificar que solicita el Cliente' :
      '';
  }
  //solCaso.registerOnDisabledChange();

  manNombre = new FormControl('', [Validators.required]);
  manNombreError() {
    return this.manNombre.hasError('required') ? 'You must enter a value' :
        this.manNombre.hasError('email') ? 'Not a valid email' :
            '';
          }
  manEmail = new FormControl('', [Validators.required, Validators.email]);
  manEmailError() {
    return this.manEmail.hasError('required') ? 'You must enter a value' :
        this.manEmail.hasError('email') ? 'Not a valid email' :
            '';
          }

  houston_estamos_listos(): boolean {
    if( this.cliNombre.valid &&
        this.solSitio.valid &&
        this.solCaso.valid &&
        this.manNombre.valid &&
        this.manEmail.valid
      ) return true;
    else return false;
  }
/* CONTROL DE APERTURA DE ACORDIOANES */
  step = 0;
  todos = 0;
  revision: boolean = true;
  cancelarLabel: string = "Editar";
  leerSolo: boolean = false;

  setOpenAll (index: number) {
    if(this.houston_estamos_listos()){
      this.step = index;
      this.todos = 1;
      this.revision = false;
      this.leerSolo = true;
      this.solCaso.disable();
      }
  }

  setStep(index: number) {
   this.step = index;
   //console.log("step: "+this.step+"  todos: "+this.todos+" "+ (this.todos === 1));
  }

  nextStep() {
   this.step++;
   this.todos = 0;
  }

  prevStep() {
   this.step--;
   this.todos = 0;
  }

  editar() {
    this.revision = true;
    this.todos = 0;
    this.step = 0;
    this.leerSolo = false;
    this.solCaso.enable();
  }
/* VALORES DE INPUTS OPCIONES */
  selectedValue: string;
  respuestas: Respuesta[] = [
    {value: '1', viewValue: 'Sin definir'},
    {value: '2', viewValue: 'Remoto'},
    {value: '3', viewValue: 'Terreno'}
  ];



  constructor(private fb: FormBuilder) { }


/* FUNCIONES DE CONTROL DE FORM */
  updateProfile() {
    this.OTForm.patchValue({

    });
  }

  addAlias() {
    this.aliases.push(this.fb.control(''));
  }

  onSubmit() {
    console.warn(this.OTForm.value);
  }

  ngOnInit() {

  }

}
