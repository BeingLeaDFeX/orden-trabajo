import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtNew2Component } from './ot-new2.component';

describe('OtNew2Component', () => {
  let component: OtNew2Component;
  let fixture: ComponentFixture<OtNew2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtNew2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtNew2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
