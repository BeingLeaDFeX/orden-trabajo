import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { OTMis } from './ot-mis';
import { HttpErrorHandler, HandleError } from '../http/http-error-handler.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class OTMisService {
  Url = 'api/misOTs';  // URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler
  ) {
    this.handleError = httpErrorHandler.createHandleError('MisOTsService');
  }

  getOTs (): Observable<OTMis[]> {
    return this.http.get<OTMis[]>(this.Url)
      .pipe(
        catchError(this.handleError('getOTs', []))
      );
  }





}
