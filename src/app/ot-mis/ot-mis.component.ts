import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

import { HttpMessageService } from '../http/http-message.service';
import { OTMis } from './ot-mis';
import { OTMisService } from './ot-mis.service';

/*
export interface MisOTs {
  codigo: string;
  position: number;
  fecha: string;
  estado: string;
}

const ELEMENT_DATA: MisOTs[] = [
  {position: 1, codigo: '1905-125', fecha: '24/05 - 9:00 AM', estado: 'Activa'},
  {position: 2, codigo: '1904-105', fecha: '13/05 - 11:20 AM', estado: 'Terminado'},
  {position: 3, codigo: '1905-156', fecha: '03/05 - 16:30 AM', estado: 'Anulada'}
];*/

@Component({
  selector: 'ot-mis',
  templateUrl: './ot-mis.component.html',
  providers: [
    OTMisService,
    HttpMessageService
  ],
  styleUrls: ['./ot-mis.component.scss']
})
export class OtMisComponent implements OnInit {
  otmis: OTMis[];

  displayedColumns: string[] =
    [
      'evento_id',
      'created_on',
/*    'last_mod',
      'ots_cod',
      'ots_created_on',
      'ots_last_mod',
      'ots_estado',
    */
      'cliente_id',
  /*  'mandante_id',
      'usuario_id',

      'solicitud_id',
      'sol_created_on',
      'sol_last_mod',
      'contrato_id',
      'sitio',
      'solicitud',
      'sol_estado',

      'respuesta_id',
      'rpta_created_on',
      'rpta_last_mod',
      'tipo',
      'personal',
      'herramientas',
      'observacion',
      'gasto',*/
      'rpta_estado'
    ];

  constructor(
    private otmisService: OTMisService
  ) { }

  ngOnInit() {

    this.getOTMis();
    console.log(this.otmis);
  }

  getOTMis(): void {
    this.otmisService.getOTs()
      .subscribe(otmis => this.otmis = otmis);
  }

}
