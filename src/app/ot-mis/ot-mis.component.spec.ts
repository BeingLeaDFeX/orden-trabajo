import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtMisComponent } from './ot-mis.component';

describe('OtMisComponent', () => {
  let component: OtMisComponent;
  let fixture: ComponentFixture<OtMisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtMisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtMisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
