export interface OTMis {
  evento_id: number;
  created_on: any;
  last_mod: any;
  //
  ots_cod: string;
  ots_created_on: any;
  ots_last_mod: any;
  ots_estado: string;
  //
  cliente_id: string;
  mandante_id: string;
  usuario_id: string;
  //
  solicitud_id: string;
  sol_created_on: any;
  sol_last_mod: any;
  contrato_id: string;
  sitio: string;
  solicitud: Text;
  sol_estado: string;
  //
  respuesta_id: string;
  rpta_created_on: any;
  rpta_last_mod: any;
  tipo: string;
  personal: string;
  herramientas: string;
  observacion: Text;
  gasto: string;
  rpta_estado: string;
}
