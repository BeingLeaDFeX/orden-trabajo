import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

export interface Comuna {
  comuna: string;
  region: string;
}

export interface Pais {
  nombre: string;
  regiones: string[]
}

@Injectable()
export class GeoPoliticService {
  regionesgUrl = 'assets/geo-cl-regiones.json';
  comunasUrl = 'assets/geo-cl-comunas.json';

  constructor(private http: HttpClient) { }

  getRegiones() {
    return this.http.get<Pais>(this.regionesgUrl)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }

  getComunas() {
    return this.http.get<Comuna>(this.comunasUrl)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };


}
