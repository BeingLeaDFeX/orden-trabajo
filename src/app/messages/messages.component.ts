import { Component, OnInit } from '@angular/core';
import { HttpMessageService } from '../http/http-message.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  constructor(public messageService: HttpMessageService) { }

  ngOnInit() {
  }

}
