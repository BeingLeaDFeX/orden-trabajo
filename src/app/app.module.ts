import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {
  HttpClientModule,
  HttpClientXsrfModule,
  HTTP_INTERCEPTORS
  } from '@angular/common/http';//forms
//import { HttpModule } from '@angular/http';
//import { HttpClientXsrfModule } from '@angular/common/http';
import { RequestCache, RequestCacheWithMap } from './ot-new/request-cache.service';
import { HttpErrorHandler } from './http/http-error-handler.service';
import { HttpMessageService } from './http/http-message.service';
import { ConfigService } from './ot-new/config.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';//forms
import { MatNativeDateModule } from '@angular/material';//forms

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from './material-module'; //Contiene componentes material para evitar desordenar aqui

import { httpInterceptorProviders } from './http-interceptors/index';

import { MainNavComponent } from './main-nav/main-nav.component'; //Barra navegacion
import { LayoutModule } from '@angular/cdk/layout'; //Requerido para funcionar
import { SheetOTComponent } from './sheet-ot/sheet-ot.component';
import { OtCardComponent } from './ot-card/ot-card.component';
import { LimitToPipe } from './pipes/limit-to.pipe';
import { OtNewComponent,
         DialogOverviewOTCreated
       } from './ot-new/ot-new.component';
import { OtMisComponent } from './ot-mis/ot-mis.component';
import { OtCompletadaComponent } from './ot-completada/ot-completada.component';
import { OtNew2Component } from './ot-new2/ot-new2.component';
import { MessagesComponent } from './messages/messages.component';
import { UsersComponent } from './users/users.component';
//import { AuthGuard,
//         AutenticacionService } from './login';
import { LoginComponent,
         JwtInterceptor,
         ErrorInterceptor,
         fakeBackendProvider
} from './login';
import { HomeComponent } from './home/home.component';
import { TestComponent } from './test/test.component';


@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    SheetOTComponent,
    OtCardComponent,
    LimitToPipe,
    OtNewComponent,
    OtMisComponent,
    OtCompletadaComponent,
    OtNew2Component,
    DialogOverviewOTCreated,
    MessagesComponent,
    UsersComponent,
    LoginComponent,
    HomeComponent,
    TestComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'My-Xsrf-Cookie',
      headerName: 'My-Xsrf-Header',
    }),
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,

    MaterialModule
  ],
  entryComponents: [
    DialogOverviewOTCreated
  ],
  exports: [
    MaterialModule
  ],

  providers: [
//    AuthGuard,
//    AutenticacionService,
    ConfigService,
    HttpErrorHandler,
    HttpMessageService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    fakeBackendProvider,
    //{ provide: RequestCache, useClass: RequestCacheWithMap },
    httpInterceptorProviders,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
