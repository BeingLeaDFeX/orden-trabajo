import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import {enableProdMode} from '@angular/core';
// Express Engine
import {ngExpressEngine} from '@nguniversal/express-engine';
// Import module map for lazy loading
import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';

import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as jwt from 'jsonwebtoken'; // jwt token
import * as fs from 'fs';
import {join} from 'path';

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();
// Get all the exported functions
const db = require('./backend/queries.js');
const otdb = require('./backend/OTqueries.js');
const RSA_PUBLIC_KEY = fs.readFileSync('./backend/public.pem');
const expressJwt = require('express-jwt');
// Express server
const app = express();

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

const PORT = process.env.PORT || 4000;
const DIST_FOLDER = join(process.cwd(), 'dist/browser');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('./dist/server/main');

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', DIST_FOLDER);

// Example Express Rest API endpoints
//app.get('/api/**', (req, res) => {
  //res.status(404).send('data requests are not supported');

//});
app.get('/api/misOTs', otdb.getOTs);
app.post('/api/neworder', otdb.createCliente);

app.get('/api/home', expressJwt({ secret: RSA_PUBLIC_KEY }), otdb.getUsersHome);

app.get('/api/login', otdb.getUsuarios);

app.post('/api/login', otdb.authenticate);
// Set the HTTP request method, the endpoint URL path, and the relevant function.
app.get('/api/users', db.getUsers);
app.get('/api/users/:id', db.getUserById);
app.post('/api/users', db.createUser);
app.put('/api/users/:id', db.updateUser);
app.delete('/api/users/:id', db.deleteUser);

// Server static files from /browser
app.get('*.*', express.static(DIST_FOLDER, {
  maxAge: '1y'
}));

// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render('index', { req });
});



// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node Express server listening on http://localhost:${PORT}`);
});
