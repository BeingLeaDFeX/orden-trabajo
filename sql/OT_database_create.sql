-- CREATE DATABASE OT_database
--   WITH OWNER pgadmin
--   TEMPLATE template0
--   ENCODING 'SQL_ASCII'
--   TABLESPACE  pg_default
--   CONNECTION LIMIT  -1;*/

drop database if exists ot_database;

CREATE DATABASE ot_database
  WITH OWNER postgres;

\connect 'ot_database'

CREATE TABLE usuarios(
  user_id SERIAL PRIMARY KEY,
  username VARCHAR (50) UNIQUE NOT NULL,
  password VARCHAR (50) NOT NULL,
  email VARCHAR (355) UNIQUE NOT NULL,
  created_on TIMESTAMP NOT NULL,
  last_login TIMESTAMP,
  nombres VARCHAR (50) NOT NULL,
  apellios VARCHAR (50) NOT NULL,
  rut VARCHAR (15),
  tel1 VARCHAR (15),
  tel2 VARCHAR (15),
  correo1 VARCHAR (50),
  correo2 VARCHAR (50),
);

CREATE TABLE clientes(
  cliente_id SERIAL PRIMARY KEY,
  nombre VARCHAR (50) NOT NULL,
  rut VARCHAR (15),
  direccion VARCHAR (50),
  comuna VARCHAR (50),
  tel1 VARCHAR (15),
  tel2 VARCHAR (15),
  tel3 VARCHAR (15),
  web1 VARCHAR (15),
  web2 VARCHAR (15),
  web3 VARCHAR (15),
  sitio_id VARCHAR (25),
  sitio_dir VARCHAR (50),
  sitio_cmna VARCHAR (20),
  sitio_tel1 VARCHAR (15),
  sitio_tel2 VARCHAR (15),
  sitio_tel3 VARCHAR (15)
);

CREATE TABLE mandantes(
  mandante_id SERIAL PRIMARY KEY,
  nombres VARCHAR (50),
  apellidos VARCHAR (50),
  cliente_id VARCHAR (20),
  correo1 VARCHAR (50),
  correo2 VARCHAR (50),
  correo3 VARCHAR (50),
  tel1 VARCHAR (15),
  tel2 VARCHAR (15),
  tel3 VARCHAR (15),
  tel4 VARCHAR (15),
  tel5 VARCHAR (15)
);

CREATE TABLE ots(
  evento_id SERIAL PRIMARY KEY,
  created_on TIMESTAMP NOT NULL,
  last_mod TIMESTAMP,
--
  ots_cod VARCHAR (30) NOT NULL,
  ots_created_on TIMESTAMP,
  ots_last_mod TIMESTAMP,
  ots_estado VARCHAR (50),
--
  cliente_id VARCHAR (50),
  mandante_id VARCHAR (10),
  usuario_id VARCHAR (10),
--
  solicitud_id VARCHAR (10),
  sol_created_on TIMESTAMP,
  sol_last_mod TIMESTAMP,
  contrato_id VARCHAR (50),
  sitio VARCHAR (50),
  solicitud VARCHAR (300),
  sol_estado VARCHAR (20),
--
  respuesta_id VARCHAR (10),
  rpta_created_on TIMESTAMP,
  rpta_last_mod TIMESTAMP,
  tipo VARCHAR (10),
  personal VARCHAR (200),
  herramientas VARCHAR (200),
  observacion VARCHAR (300),
  gasto VARCHAR (100),
  rpta_estado VARCHAR (20)
);
