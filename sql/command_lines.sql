INSERT INTO ots (
  evento_id,
  created_on,
  last_mod,
--
  ots_cod,
  ots_created_on,
  ots_last_mod,
  ots_estado,
--
  cliente_id,
  mandante_id,
  usuario_id,
--
  solicitud_id,
  sol_created_on,
  sol_last_mod,
  contrato_id,
  sitio,
  solicitud,
  sol_estado,
  --
  respuesta_id,
  rpta_created_on,
  rpta_last_mod,
  tipo,
  personal,
  herramientas,
  observacion,
  gasto,
  rpta_estado
)

VALUES (
  999,
  "test1",
  "test2",
--
  "test3",
  "test4",
  "test5",
  "test6",
--
  "test7",
  "test8",
  "test9",
--
  "test10",
  "test11",
  "test12",
  "test13",
  "test14",
  "test15",
  "test16",
  --
  "test17",
  "test18",
  "test19",
  "test20",
  "test21",
  "test22",
  "test23",
  "test24",
  "test25",
);

INSERT INTO ots
generate_series(1,10) AS evento_id,
md5(random()::text) AS created_on,
md5(random()::text) AS last_mod,
--
md5(random()::text) AS ots_cod,
md5(random()::text) AS ots_created_on,
md5(random()::text) AS ots_last_mod,
md5(random()::text) AS ots_estado,
--
md5(random()::text) AS cliente_id,
md5(random()::text) AS mandante_id,
md5(random()::text) AS usuario_id,
--
md5(random()::text) AS solicitud_id,
md5(random()::text) AS sol_created_on,
md5(random()::text) AS sol_last_mod,
md5(random()::text) AS contrato_id,
md5(random()::text) AS sitio,
md5(random()::text) AS solicitud,
md5(random()::text) AS sol_estado,
--
md5(random()::text) AS respuesta_id,
md5(random()::text) AS rpta_created_on,
md5(random()::text) AS rpta_last_mod,
md5(random()::text) AS tipo,
md5(random()::text) AS personal,
md5(random()::text) AS herramientas,
md5(random()::text) AS observacion,
md5(random()::text) AS gasto,
md5(random()::text) AS rpta_estado;


INSERT INTO ots ( evento_id, created_on, last_mod, ots_cod, ots_created_on, ots_last_mod, ots_estado, cliente_id,
  mandante_id,
  usuario_id,
--
  solicitud_id,
  sol_created_on,
  sol_last_mod,
  contrato_id,
  sitio,
  solicitud,
  sol_estado,
  --
  respuesta_id,
  rpta_created_on,
  rpta_last_mod,
  tipo,
  personal,
  herramientas,
  observacion,
  gasto,
  rpta_estado
)

VALUES (
  999,
  "test1",
  "test2",
--
  "test3",
  "test4",
  "test5",
  "test6",
--
  "test7",
  "test8",
  "test9",
--
  "test10",
  "test11",
  "test12",
  "test13",
  "test14",
  "test15",
  "test16",
  --
  "test17",
  "test18",
  "test19",
  "test20",
  "test21",
  "test22",
  "test23",
  "test24",
  "test25",
);


curl --data "name=Javier&email=javier@example.com" http://localhost:4000/api/users


curl -X PUT -d "name=Kramer" -d "email=kramer@example.com" http://localhost:4000/api/users/1

Cliente:
  comuna: ""
  direccion: ""
  nombre: ""
  web: ""

Mandante:
  correo: ""
  direccion: ""
  nombre: ""
  telefono: ""

Respuesta:
  agenda: ""
  herramientas: ""
  personal: ""
  tipo: ""

Solicitud:
  caso: ""
  contrato: ""
  sitio: ""
