const jwt = require('jsonwebtoken');
const fs = require('fs');
const RSA_PRIVATE_KEY = fs.readFileSync('./backend/private.pem');
const RSA_PUBLIC_KEY = fs.readFileSync('./backend/public.pem');

/*const expressJwt = require('express-jwt');

const checkIfAuthenticated = expressJwt({
    secret: RSA_PUBLIC_KEY
});
/*
app.get('/protected', jwt({secret: 'shhhhhhared-secret'}),
  function(req, res) {
    if (!req.user.admin) return res.sendStatus(401);
    res.sendStatus(200);
  });

const checkIfLogged = ( token ) => {

}  */

var pg2 = require('pg');
const Pool2 = pg2.Pool;
const pool2 = new Pool2({
  user: 'maxwell',
  host: 'localhost',
  database: 'ot_database',
  password: 'jugabaStarcraftenelCEC',
  port: 5432,
})

// GET all users ordered by id
const getOTs = (request, response) => {
  pool2.query('SELECT * FROM ots ORDER BY evento_id ASC', (error, results) => {
    if (error) {
      throw error
    }
    console.log(results.rows);
    response.status(200).json(results.rows)
  })
}

// POST a new user
const createCliente = (request, response) => {
  const { nombre, rut, direccion, comuna, tel1, tel2, tel3,
          web1, web2, web3, sitio_id, sitio_dir, sitio_cmna,
          sitio_tel1, sitio_tel2, sitio_tel3 } = request.body
  var query = {
    name: 'Crear-cliente',
    text: `INSERT INTO clientes (nombre, rut, direccion, comuna,
           tel1, tel2, tel3, web1, web2, web3, sitio_id, sitio_dir,
           sitio_cmna, sitio_tel1, sitio_tel2, sitio_tel3)
           VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11,
           $12, $13, $14, $15, $16)`,
    values: [ nombre, rut, direccion, comuna, tel1, tel2, tel3, web1,
              web2, web3, sitio_id, sitio_dir, sitio_cmna, sitio_tel1,
              sitio_tel2, sitio_tel3]
  };
  pool2.query(query, (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`User added with ID: ${results.insertId}`)
  })
}

// route functions
const authenticate = (request, response) => {

  const { _username, _password } = request.body;
  var query = {
    name: 'validate-user',
    text: `SELECT user_id, email, last_login, nombres, apellidos, tel1
           FROM usuarios WHERE username = $1::text AND password = $2::text`,
    values: [ _username, _password ], //rowMode: 'array'
  };
  pool2.query(query, (error, results) => {
    console.log("backend authenticate() query: error "+error);
    if (error) {
      throw error
    }
    var respuesta = JSON.parse(JSON.stringify(results.rows));
    /*      var aux = respuesta[0]['username'];
      console.log("backend authenticate() results.rows : "+results.rows);
      console.log("");
      console.log("backend authenticate() aux : "+ JSON.stringify(repuesta));
      console.log("");
      console.log("backend authenticate() rowCount: "+results.rowCount);
      console.log("________________________________");*/
    if( results.rows[0] === undefined ) {
      console.log("backend authenticate(): Username or password is incorrect");
      return response.status(401).send('Username or password is incorrect');
    }
    else {
      const jwtBearerToken = jwt.sign({ nombre: 'selitron' }, RSA_PRIVATE_KEY, {
                algorithm: 'RS256',
                expiresIn: 120,
                subject: (respuesta[0]['user_id']).toString()
            });


      respuesta[0]['token'] = jwtBearerToken;
      //console.log("backend authenticate() respuesta : "+ respuesta)
      response.status(200).send(respuesta);
      //response.status(200).json([results.rows, jwtBearerToken ]);
    }

  })
}


const getUsuarios = (request, response) => {

  const { method, url, headers } = request;
  console.log("backend getUsuarios() url: "+url);
  console.log("backend getUsuarios() method: "+method);
  console.log("backend getUsuarios() headers: "+headers);
/*  expressJwt(RSA_PRIVATE_KEY);
  if (!req.user.admin){
    return res.sendStatus(401);
  }*/
    var query = {
      name: 'get-usuarios',
      text: `SELECT user_id, nombre, apellido, email FROM usuarios`
    };

    pool2.query(query, (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(results.rows);
    })
}
/* HomeComponent */
const getUsersHome = (req, res) => {
  var query = {
    name: 'get-usersHome',
    text: `SELECT user_id, email, last_login, nombres,
           apellidos, tel1 FROM usuarios ORDER BY apellidos ASC`
  };
  pool2.query(query, (error, results) => {
    if (error) {
      throw error
    }
    console.log(JSON.stringify(results.rows));
    res.status(200).json(results.rows)
  })
}


/*
const isLoggedIn() {
  response headers.get('Authorization')
}

*/


module.exports = {
  getOTs,
  createCliente,
  authenticate,
  getUsuarios,
  getUsersHome
}
