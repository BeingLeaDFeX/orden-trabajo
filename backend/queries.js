var pg = require('pg');
//delete pg.native;

const Pool = pg.Pool;
//const Pool = require('pg').Pool
const pool = new Pool({
  user: 'maxwell',
  host: 'localhost',
  database: 'api',
  password: 'jugabaStarcraftenelCEC',
  port: 5432,
})

// GET all users ordered by id
const getUsers = (request, response) => {
  console.log("request: "+request)
  console.log("request.body: "+request.body)
  pool.query('SELECT * FROM users ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error

    }
    //var orden = json(results.rows);
    //response.status(200).send("seee: "+orden);
    console.log(results.rows);
    //response.status(200).JSON(results.rows);
    response.status(200).json(results.rows)
  })
}

// GET a single user by id
const getUserById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

// POST a new user
const createUser = (request, response) => {
  console.log("request: "+request)
  console.log("request.body: "+request.body)
  const { name, email } = request.body

  pool.query('INSERT INTO users (name, email) VALUES ($1, $2)', [name, email],
    (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`User added with ID: ${results.insertId}`)
  })
}

// PUT updated data in an existing user
const updateUser = (request, response) => {
  const id = parseInt(request.params.id)
  const { name, email } = request.body

  pool.query(
    'UPDATE users SET name = $1, email = $2 WHERE id = $3',
    [name, email, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User modified with ID: ${id}`)
    }
  )
}


// DELETE a user
const deleteUser = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User deleted with ID: ${id}`)
  })
}

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
}
